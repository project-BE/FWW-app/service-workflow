import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ClientsModule } from '@nestjs/microservices';
import { kafkaOptions } from './transport/kafka.config';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { BookWorkflowService } from './book-workflow/book-workflow.service';
import { HttpModule } from '@nestjs/axios';

const configService = new ConfigService();

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    ClientsModule.register(kafkaOptions),
    HttpModule.register({
      timeout: 5000,
      maxRedirects: 5,
    }),
  ],
  controllers: [AppController],
  providers: [AppService, BookWorkflowService],
})
export class AppModule {}

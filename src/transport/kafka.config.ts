import { ConfigService } from '@nestjs/config';
import {
  ClientsModuleOptions,
  KafkaOptions,
  Transport,
} from '@nestjs/microservices';
import * as dotenv from 'dotenv';

dotenv.config();

export const kafkaOptions: ClientsModuleOptions = [
  {
    name: process.env.KAFKA_TOKEN,
    transport: Transport.KAFKA,
    options: {
      client: {
        clientId: process.env.KAFKA_CLIENT_ID,
        brokers: [process.env.KAFKA_BROKERS],
      },
      consumer: {
        groupId: process.env.KAFKA_GROUPID,
      },
    },
  },
];

export const kafkaMainOptions: KafkaOptions = {
  transport: Transport.KAFKA,
  options: {
    client: {
      clientId: process.env.KAFKA_CLIENT_ID,
      brokers: [process.env.KAFKA_BROKERS],
    },
    consumer: {
      groupId: process.env.KAFKA_GROUPID,
    },
  },
};

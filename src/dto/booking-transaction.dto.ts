export class BookingTransactionDto {
  transaction_id: string;

  reservation: Reservation;

  passenger: Passenger;
}

class Reservation {
  flight_id: number;

  transaction_id: string;

  departure_time: string;

  seat_id: number;

  user_id: number;
}

class Passenger {
  first_name: string;

  last_name: string;

  email: string;

  phone_number: string;

  nik: string;
}

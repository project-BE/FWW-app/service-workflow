import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { MicroserviceOptions } from '@nestjs/microservices';
import { kafkaMainOptions } from './transport/kafka.config';
import { CorsOptions } from '@nestjs/common/interfaces/external/cors-options.interface';

async function bootstrap() {
  // const app = await NestFactory.createMicroservice<MicroserviceOptions>(
  //   AppModule,
  //   kafkaMainOptions,
  // );
  // await app.listen();

  const app = await NestFactory.create(AppModule);
  const microservice = app.connectMicroservice(kafkaMainOptions);
  await app.startAllMicroservices();

  await app.listen(3005);
}
bootstrap();

import { Inject, Injectable } from '@nestjs/common';
import { ClientKafka } from '@nestjs/microservices';
import { BookingTransactionDto } from '../dto/booking-transaction.dto';
import { HttpService } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';
import { lastValueFrom } from 'rxjs';
import { AxiosRequestConfig } from 'axios';

@Injectable()
export class BookWorkflowService {
  constructor(
    @Inject('KAFKA_SERVICE_WORKFLOW') private readonly clientKafka: ClientKafka,
    private readonly httpService: HttpService,
    private readonly configService: ConfigService,
  ) {}

  async importDynamic(modulePath) {
    return await import(modulePath);
  }

  private async sendKafkaRequest(topic: string, payload: any): Promise<any> {
    return new Promise((resolve, reject) => {
      const timeout = setTimeout(() => {
        reject(new Error(`Kafka request to ${topic} timed out`));
      }, 5000);

      this.clientKafka.send(topic, payload).subscribe({
        next: (result) => {
          clearTimeout(timeout);
          resolve(result);
        },
        error: (error) => {
          clearTimeout(timeout);
          reject(error);
        },
      });
    });
  }

  async startBookProcess(inputData) {
    try {
      // 1. get list process
      const urlGetProcessId = '/engine-rest/process-definition';
      const config: AxiosRequestConfig = {
        baseURL: this.configService.get('CAMUNDA_HOST'),
      };
      const result = await lastValueFrom(
        this.httpService.get(urlGetProcessId, config),
      );
      const filteredProcess = result.data.filter(
        (el) => el.key === this.configService.get('BOOK_WORKFLOW_PROCESS_KEY'),
      );
      const processId = filteredProcess[0].id;

      // 3. start process and set variables
      const urlStartProcess = `${this.configService.get(
        'CAMUNDA_HOST',
      )}/engine-rest/process-definition/${processId}/start`;

      const payload = {
        variables: {
          processVariables: {
            value: JSON.stringify(inputData),
            type: 'String',
          },
          inputFlightDate: {
            value: inputData.reservation['departure_time'],
            type: 'String',
          },
          totalBuyPerDay: {
            value: inputData.reservation['user_transaction_today'],
            type: 'Integer',
          },
          totalNumberPerFlight: {
            value: inputData.reservation['user_in_flight'],
            type: 'Integer',
          },
          userBlackListStatus: {
            value: inputData.reservation['user_blacklist_status'],
            type: 'Boolean',
          },
          dukcapilStatus: {
            value: inputData.passenger['status_dukcapil'],
            type: 'Boolean',
          },
          peduliLindungiStatus: {
            value: inputData.passenger['status_pedulilindungi'],
            type: 'Boolean',
          },
        },
      };

      const startProcess = await lastValueFrom(
        this.httpService.post(urlStartProcess, payload),
      );

      return startProcess;
    } catch (error) {
      console.error(
        'Error from workflow service:',
        [error],
        error.response?.status,
        error.response?.data,
      );
    }
  }

  async startPaymentProcess(inputData) {
    try {
      // 1. get list process
      const urlGetProcessId = '/engine-rest/process-definition';
      const config: AxiosRequestConfig = {
        baseURL: this.configService.get('CAMUNDA_HOST'),
      };
      const result = await lastValueFrom(
        this.httpService.get(urlGetProcessId, config),
      );
      const filteredProcess = result.data.filter(
        (el) =>
          el.key === this.configService.get('PAYMENT_WORKFLOW_PROCESS_KEY'),
      );
      const processId = filteredProcess[0].id;

      // 3. start process and set variables
      const urlStartProcess = `${this.configService.get(
        'CAMUNDA_HOST',
      )}/engine-rest/process-definition/${processId}/start`;

      const payload = {
        variables: {
          processPaymentVariables: {
            value: JSON.stringify(inputData),
            type: 'String',
          },
        },
      };

      const startProcess = await lastValueFrom(
        this.httpService.post(urlStartProcess, payload),
      );

      return startProcess;
    } catch (error) {
      console.log([error], ' error workflow payment');
    }
  }

  async startTicketRedeemProcess(inputData) {
    try {
      // 1. get list process
      const urlGetProcessId = '/engine-rest/process-definition';
      const config: AxiosRequestConfig = {
        baseURL: this.configService.get('CAMUNDA_HOST'),
      };
      const result = await lastValueFrom(
        this.httpService.get(urlGetProcessId, config),
      );
      const filteredProcess = result.data.filter(
        (el) => el.key === this.configService.get('TICKET_REDEEM_PROCESS_KEY'),
      );
      const processId = filteredProcess[0].id;

      // 3. start process and set variables
      const urlStartProcess = `${this.configService.get(
        'CAMUNDA_HOST',
      )}/engine-rest/process-definition/${processId}/start`;

      const payload = {
        variables: {
          processRedeemTicketVariables: {
            value: JSON.stringify(inputData),
            type: 'String',
          },
        },
      };

      const startProcess = await lastValueFrom(
        this.httpService.post(urlStartProcess, payload),
      );

      return startProcess;
    } catch (error) {
      console.log([error], ' error workflow payment');
    }
  }
}

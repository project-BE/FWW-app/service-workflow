import { Test, TestingModule } from '@nestjs/testing';
import { BookWorkflowService } from './book-workflow.service';

describe('BookWorkflowService', () => {
  let service: BookWorkflowService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BookWorkflowService],
    }).compile();

    service = module.get<BookWorkflowService>(BookWorkflowService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

import {
  Inject,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { BookingTransactionDto } from './dto/booking-transaction.dto';
import { ClientKafka } from '@nestjs/microservices';
import { BookWorkflowService } from './book-workflow/book-workflow.service';
import { ConfigService } from '@nestjs/config';
import { lastValueFrom } from 'rxjs';
import { HttpService } from '@nestjs/axios';
import * as crypto from 'crypto';

@Injectable()
export class AppService {
  constructor(
    private readonly bookWorkflowService: BookWorkflowService,
    private readonly configService: ConfigService,
    private readonly httpService: HttpService,
    @Inject('KAFKA_SERVICE_WORKFLOW') private readonly clientKafka: ClientKafka,
  ) {}

  async importDynamic(modulePath) {
    return await import(modulePath);
  }

  private sendKafkaRequest(topic: string, payload: any): Promise<any> {
    return new Promise((resolve, reject) => {
      const timeout = setTimeout(() => {
        reject(new Error(`Kafka request to ${topic} timed out`));
      }, 5000);

      this.clientKafka.send(topic, payload).subscribe({
        next: (result) => {
          clearTimeout(timeout);
          resolve(result);
        },
        error: (error) => {
          clearTimeout(timeout);
          reject(error);
        },
      });
    });
  }

  async bookTransaction(data: BookingTransactionDto) {
    try {
      // Get user transaction for today
      const checkUserTransaction = await this.sendKafkaRequest(
        'check-user-transaction',
        {
          key: data.transaction_id,
          value: JSON.stringify(data),
        },
      );
      data.reservation['user_transaction_today'] =
        checkUserTransaction.reservation['user_transaction_today'];

      // Get user transaction in the chosen flight
      const checkUserFlight = await this.sendKafkaRequest('check-user-flight', {
        key: data.transaction_id,
        value: JSON.stringify(data),
      });
      data.reservation['user_in_flight'] =
        checkUserFlight.reservation['user_in_flight'];

      // Get user blacklist status
      const checkBlacklist = await this.sendKafkaRequest('check-blacklist', {
        key: data.transaction_id,
        value: JSON.stringify(data),
      });
      data.reservation['user_blacklist_status'] =
        checkBlacklist.reservation['user_blacklist_status'];

      // Get peduli lindungi status
      const urlPeduliLindungi = `${this.configService.get(
        'PEDULI_LINDUNGI_HOST',
      )}/m1/419308-0-default/peduli-lindungi/eligibility`;
      const payloadPeLin = {
        nik: data.passenger.nik,
        first_name: data.passenger.first_name,
        last_name: data.passenger.last_name,
      };
      const statusPeduliLindungi = await lastValueFrom(
        this.httpService.post(urlPeduliLindungi, payloadPeLin),
      );
      data.passenger['status_pedulilindungi'] =
        statusPeduliLindungi.data.data.valid;

      // Get dukcapil status
      const urlDukcapil = `${this.configService.get(
        'DUKCAPIL_HOST',
      )}/m1/419308-0-default/dukcapil/nik`;
      const payloadDukcapil = {
        nik: data.passenger.nik,
        first_name: data.passenger.first_name,
        last_name: data.passenger.last_name,
      };
      const statusDukcapil = await lastValueFrom(
        this.httpService.post(urlDukcapil, payloadDukcapil),
      );
      data.passenger['status_dukcapil'] = statusDukcapil.data.data.valid;

      // generate booking code
      const bookingCode = this.generateFlightBookingCode(
        data.reservation.user_id,
        data.reservation.seat_id,
      );
      data.reservation['booking_code'] = bookingCode;

      // date string conversion
      data.reservation.departure_time = new Date(
        data.reservation.departure_time,
      ).toISOString();

      // initiate workflow for book-flight-process
      const result = await this.bookWorkflowService.startBookProcess(data);
      return result;
    } catch (error) {
      console.error('Error book flight transaction:', error);
      throw new InternalServerErrorException('Error book flight transaction');
    }
  }

  async makePayment(data) {
    // get createdDate of the bookingCode
    const bookCodeCreatedDate = await this.sendKafkaRequest(
      'check-booking-timestamp',
      {
        key: data.transaction_id,
        value: JSON.stringify(data),
      },
    );
    data['payment_message']['bookingcode_date'] = bookCodeCreatedDate;

    // get price of the seat
    const seatPrice = await this.sendKafkaRequest('check-seat-price', {
      key: data.transaction_id,
      value: JSON.stringify(data),
    });
    data['payment_message']['seat_price'] = seatPrice;

    // start workflow
    const result = await this.bookWorkflowService.startPaymentProcess(data);
    return result;
  }

  async getOnBoard(data) {
    // get createdDate of the reservationCode
    const reservCodeCreatedDate = await this.sendKafkaRequest(
      'check-reservcode-timestamp',
      {
        key: data.transaction_id,
        value: JSON.stringify(data),
      },
    );
    data['rcode_date'] = reservCodeCreatedDate['created_at'];
    data['departure_time'] = reservCodeCreatedDate['departure_time'];

    // get ticket detail
    const ticketDetail = await this.sendKafkaRequest('get-ticket-detail', {
      key: data.transaction_id,
      value: JSON.stringify(data),
    });
    const ticketDetailObj = {
      email: ticketDetail.email,
      first_name: ticketDetail.first_name,
      last_name: ticketDetail.last_name,
      flight_id: ticketDetail.flight_id,
      departure_airport_name: ticketDetail.departure_airport_name,
      destination_airport_name: ticketDetail.destination_airport_name,
      seat_number: ticketDetail.seat_number,
    };
    data['ticket_detail'] = ticketDetailObj;

    // start workflow
    const result =
      await this.bookWorkflowService.startTicketRedeemProcess(data);
    return result;
  }

  private generateFlightBookingCode(userId, seatId) {
    const dataToHash = `${userId}-${seatId}-${Date.now()}`;

    // Use SHA-256 hash function to create a unique hash
    const hash = crypto.createHash('sha256').update(dataToHash).digest('hex');

    // Take the first 8 characters of the hash as the booking code
    const bookingCode = hash.substring(0, 8).toUpperCase();

    return bookingCode;
  }

  onModuleInit() {
    this.clientKafka.subscribeToResponseOf('check-user-transaction');
    this.clientKafka.subscribeToResponseOf('check-user-flight');
    this.clientKafka.subscribeToResponseOf('check-blacklist');
    this.clientKafka.subscribeToResponseOf('check-booking-timestamp');
    this.clientKafka.subscribeToResponseOf('check-seat-price');
    this.clientKafka.subscribeToResponseOf('check-reservcode-timestamp');
    this.clientKafka.subscribeToResponseOf('get-ticket-detail');
  }
}

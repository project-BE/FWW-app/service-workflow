import { Controller, Get, Inject } from '@nestjs/common';
import { AppService } from './app.service';
import { ClientKafka, EventPattern, Payload } from '@nestjs/microservices';
import { BookingTransactionDto } from './dto/booking-transaction.dto';
import { BookWorkflowService } from './book-workflow/book-workflow.service';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly bookWorkflowService: BookWorkflowService,
    @Inject('KAFKA_SERVICE_WORKFLOW') private readonly clientKafka: ClientKafka,
  ) {}

  @EventPattern('booking-transaction')
  async bookTransaction(@Payload() data: BookingTransactionDto) {
    await this.appService.bookTransaction(data);
  }

  @EventPattern('payment-transaction')
  async makePayment(@Payload() data) {
    await this.appService.makePayment(data);
  }

  @EventPattern('boarding-transaction')
  async getOnBoard(@Payload() data){
    await this.appService.getOnBoard(data)

  }
}
